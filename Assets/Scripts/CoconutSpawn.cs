using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine;
using System.Collections;

public class CoconutSpawn : MonoBehaviour
{

    private AsyncOperationHandle<GameObject> operationHandle;
    private GameObject obj;
    private Vector3 _offset = new Vector3(0, 0, 33);

    void SpawnCoconut()
    {
        operationHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/Coconut.prefab"); //spawn enviro
        operationHandle.Completed += AsyncOperationHandle_Completed;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SpawnCoconut();
        }
    }


    private void AsyncOperationHandle_Completed(AsyncOperationHandle<GameObject> asyncOperationHandle)
    {
        if (asyncOperationHandle.Status == AsyncOperationStatus.Succeeded)
        {
            obj = Instantiate(asyncOperationHandle.Result);
            obj.transform.position = PlayerController.Instance.GetPlayerPosition() + _offset; //set enviro position
        }
        else
        {
            Debug.Log("Failed to load!");
        }
    }

    private IEnumerator Spawning()
    {
        InvokeRepeating("SpawnCoconut", 2, 3);
        yield return new WaitForSeconds(15.5f);
        InvokeRepeating("SpawnCoconut", 0, 3);
        yield return new WaitForSeconds(12.75f);
        InvokeRepeating("SpawnCoconut", 0, 3);
    }

    public void StartSpawningCoconut()
    {
        StartCoroutine(Spawning());
    }

    public void StopSpawningCoconut()
    {
        CancelInvoke("SpawnCoconut");
        StopAllCoroutines();
    }
}
