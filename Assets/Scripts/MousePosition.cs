using UnityEngine;

public class MousePosition : MonoBehaviour
{
    [SerializeField] private LayerMask mousePlaneLayerMask;


    public float MousePositionX()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue, mousePlaneLayerMask);
        return raycastHit.point.x;
    }
}
