using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;


    [SerializeField] private int amount = 5;
    [SerializeField] private MousePosition mousePosition;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject shield;

    [SerializeField] private AudioClip showBubble;
    [SerializeField] private AudioClip hideBubble;

    private Vector3 _startPosition;
    private bool _isAlive = false;
    float _lastUpdate;
    private bool _isFlying = false;

    private void Awake()
    {

        if (Instance != null)
        {
            Debug.LogError("There's more then one PlayerController! " + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {

        _startPosition = transform.position;
        _lastUpdate = Time.time;
    }

    void Update()
    {
        if (_isAlive)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * 2, Space.World);
            PlayerMove();

            if (Time.time - _lastUpdate >= 1f)
            {
                ScoreController.Instance.AddScore(amount);
                _lastUpdate = Time.time;
            }
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (_isFlying)
            {
                GetComponent<Animator>().SetTrigger("land");
                _isFlying = false;
            }
            else
            {
                GetComponent<Animator>().SetTrigger("fly");
                _isFlying = true;
            }

        }

    }
    void PlayerMove()
    {
        Vector3 pos = new Vector3(mousePosition.MousePositionX(), transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime*2);
    }

    private void OnCollisionEnter(Collision collision)
    {
       // Debug.Log(collision.transform.name);
        if(collision.transform.name.Contains("Coconut"))
        {
            gameOverPanel.SetActive(true);
            GetComponent<AudioSource>().Stop();
            _isAlive = false;
            GetComponent<Animator>().speed = 0;
            collision.transform.GetComponent<Coconut>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            AudioController.Instance.WaitMusic();
            //TODO: destroy all coconuts on scene
        }
        if (collision.transform.name.Contains("ToucanShield"))
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
        }
    }

    public void ResetPlayer()
    {
        transform.position = _startPosition;
        GetComponent<Animator>().speed = 1;

        _isAlive = true;
        GetComponent<AudioSource>().Play();
        GetComponent<BoxCollider>().enabled = true;
    }

    public Vector3 GetPlayerPosition()
    {
        return transform.position;
    }

    public void RunPlayer()
    {
        gameObject.GetComponent<Animator>().enabled = true;
        _isAlive = true;
        GetComponent<AudioSource>().Play();
    }

    public void ActivateShield()
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        shield.SetActive(true);
        shield.GetComponent<AudioSource>().PlayOneShot(showBubble);

    }


    public void DeactivateShield()
    {
        shield.GetComponent<AudioSource>().PlayOneShot(hideBubble);
        shield.SetActive(false);
        gameObject.GetComponent<BoxCollider>().enabled = true;
    }
}
