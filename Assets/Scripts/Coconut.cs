using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coconut : MonoBehaviour
{
    public  Vector3 _velocity;
    float _vel;

    private void OnEnable()
    {
        _vel = Random.RandomRange(4, 20);
        _velocity = new Vector3(0, 0, -_vel);
        StartCoroutine(DestroyCoconut());
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name.Contains("Cube"))
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
        }

        Debug.Log(collision.transform.name); ;

        if (collision.transform.name.Contains("ShieldToucan"))
        {
            Debug.Log("dsada");
            PlayerController.Instance.DeactivateShield();
            Destroy(gameObject);

        }
    }
    void Update()
    {
        GetComponent<Rigidbody>().velocity = _velocity;
    } 

    IEnumerator DestroyCoconut()
    {
        yield return new WaitForSeconds(8);
        Destroy(gameObject);
    }
}
