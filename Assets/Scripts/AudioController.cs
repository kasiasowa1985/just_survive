using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [SerializeField] private AudioClip waitMusic;
    [SerializeField] private AudioClip playMusic;
    [SerializeField] private AudioClip gameOver;

    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more then one AudioController! " + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }
    public void WaitMusic()
    {
        audioSource.PlayOneShot(gameOver);
        audioSource.clip = waitMusic;
        audioSource.Play();
    }

    public void PlayMusic()
    {        
        audioSource.clip = playMusic;
        audioSource.Play();
    }
}
