using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    [SerializeField] private Transform player;

    void LateUpdate()
    {
       transform.position = new Vector3(0, (player.position + offset).y, (player.position + offset).z);
    }
}
