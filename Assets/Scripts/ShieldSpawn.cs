using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ShieldSpawn : MonoBehaviour
{
    private AsyncOperationHandle<GameObject> operationHandle;
    private GameObject obj;
    private Vector3 _offset = new Vector3(0, 0.5f, 33);

  
    void SpawnShield()
    {
        operationHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/Shield.prefab"); //spawn enviro
        operationHandle.Completed += AsyncOperationHandle_Completed;
    }

    private void AsyncOperationHandle_Completed(AsyncOperationHandle<GameObject> asyncOperationHandle)
    {
        if (asyncOperationHandle.Status == AsyncOperationStatus.Succeeded)
        {
            obj = Instantiate(asyncOperationHandle.Result);
            obj.transform.position = PlayerController.Instance.GetPlayerPosition() + _offset; //set enviro position
        }
        else
        {
            Debug.Log("Failed to load!");
        }
    }

     public void StartSpawningShields()
    {
        InvokeRepeating("SpawnShield", 5, 30);
    }

    public void StopSpawningShields()
    {
        CancelInvoke("SpawnShield");
    }

}
