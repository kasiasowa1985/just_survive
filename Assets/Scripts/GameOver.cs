using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOver : MonoBehaviour
{
    [SerializeField] private Button playAgain;
    [SerializeField] private Button quit;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private SceneManager sceneManager;

    private void OnEnable()
    {
        scoreText.text = "You've reached: " + ScoreController.Instance.GetScore().ToString() + " score!";

        playAgain.onClick.AddListener(Reset);
        quit.onClick.AddListener(QuitApp);
        sceneManager.StopSpawn();
    }

    private void OnDisable()
    {
        playAgain.onClick.RemoveListener(Reset);
        quit.onClick.RemoveListener(QuitApp);
    }

    private void QuitApp()
    {
        Application.Quit();
    }
    private void Reset()
    {
        ScoreController.Instance.ResetScore();
        PlayerController.Instance.ResetPlayer();
        gameObject.SetActive(false);
        sceneManager.StartSpawn();
        AudioController.Instance.PlayMusic();
    }
}
