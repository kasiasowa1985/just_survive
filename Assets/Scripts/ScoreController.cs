using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    public static ScoreController Instance;
    private int _score = 0;

    [SerializeField] private TextMeshProUGUI scoreText;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more then one ScoreController! " + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    public void ResetScore()
    {
        _score = 0;
    }

    public void AddScore(int amount)
    {
        _score += amount;
        scoreText.text = "Score: " + _score.ToString();
    }

    public int GetScore()
    {
        return _score;
    }
}
