using System.Collections;
using UnityEngine;

public class Shield : MonoBehaviour
{
    float _speed = 150f;

    private void OnEnable()
    {
        StartCoroutine(DestroyShield());
    }
    private void Update()
    {
        transform.Rotate(Vector3.up * _speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.name.Contains("Toucan"))
        {
            PlayerController.Instance.ActivateShield();
            gameObject.SetActive(false);
        }
        else
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
        }
    }

    IEnumerator DestroyShield()
    {
        yield return new WaitForSeconds(20);
        Destroy(gameObject);
    }
}
