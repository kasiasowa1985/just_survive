using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class EnvitonmentSpawn : MonoBehaviour
{
    private AsyncOperationHandle<GameObject> operationHandle;
    private GameObject obj;

  
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name.Contains("Toucan"))
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            operationHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/Enviro.prefab"); //spawn enviro

            operationHandle.Completed += AsyncOperationHandle_Completed;
        }
    }

    private void AsyncOperationHandle_Completed(AsyncOperationHandle<GameObject> asyncOperationHandle)
    {
        if (asyncOperationHandle.Status == AsyncOperationStatus.Succeeded)
        {
            obj = Instantiate(asyncOperationHandle.Result);
            obj.transform.position = transform.position + new Vector3(0, -0.5f, 74.59f); //set enviro position
            StartCoroutine(DespawnEnviro()); //not working yet 
        }
        else
        {
            Debug.Log("Failed to load!");
        }
    }

    //TODO: destroy unused enviro
    private IEnumerator DespawnEnviro()
    {
        yield return new WaitForSeconds(10f);

        if (!operationHandle.IsValid())
            Addressables.Release(operationHandle);
        else
            Destroy(transform.parent.GetComponent<GameObject>());

    }
}
