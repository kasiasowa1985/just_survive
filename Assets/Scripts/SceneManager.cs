using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{
    [SerializeField] private CoconutSpawn coconutSpawn;
    [SerializeField] private ShieldSpawn shieldSpawn;
    [SerializeField] private GameObject startPanel;
    [SerializeField] private Button playOnStart;


    private void OnEnable()
    {
        playOnStart.onClick.AddListener(() =>
        {
            AudioController.Instance.PlayMusic();
            startPanel.SetActive(false);
            StartSpawn();

            PlayerController.Instance.RunPlayer();
            playOnStart.onClick.RemoveAllListeners();           
        });
    }

    public void StartSpawn()
    {
        coconutSpawn.StartSpawningCoconut();
        shieldSpawn.StartSpawningShields();
    }

    public void StopSpawn()
    {
        coconutSpawn.StopSpawningCoconut();
        shieldSpawn.StopSpawningShields();
    }
}
